# Describes a controller that operates
# buses in park and buses on route both with
# route management
class BusParkController
  attr_reader :routes, :buses_in_park, :buses_on_route

  def initialize(routes, buses)
    @routes = routes
    @buses_in_park = buses
    @buses_on_route = []
  end

  def buses_by_route
    @routes.each do |route|
      yield route, @buses_on_route.select { |bus| bus.route == route.number }
    end
  end

  def total_consumption
    cbr = consumption_by_route
    return nil if cbr.nil?
    cbr.keys.count > 0 ? consumption_by_route.each_value.reduce(:+) : nil
  end

  def consumption_by_route
    consumptions = {}
    buses_by_route do |route, buses|
      cbr = buses.map(&:petrol_consumption)
                 .reduce { |acc, c| acc + c * route.length }
      consumptions["Route #{route.number}"] = cbr.round(2) unless cbr.nil?
    end
    consumptions.keys.count > 0 ? consumptions : nil
  end

  def add_bus(bus)
    @buses_in_park << bus
    nil
  end

  def add_route(route)
    @routes << route
    nil
  end

  def find_bus(bus_number)
    matching_buses = @buses_in_park.select { |b| b.number == bus_number }
    raise(StandardError) unless matching_buses.count.zero?
    matching_buses[0]
  end

  def add_bus_on_route(bus_number)
    begin
      bus = find_bus(bus_number)
    rescue StandardError
      return 'No such bus'
    end
    d_route = @routes.select { |r| r.number == bus.route }
    d_route = d_route[0].number_of_buses
    buses_on_route = @buses_on_route.select { |b| b.route == bus.route }.count
    if buses_on_route < d_route
      @buses_on_route << bus
      @buses_in_park.delete(bus)
      nil
    else
      "Can't add bus"
    end
  end

  def remove_bus_from_route(bus_number)
    begin
      bus = find_bus(bus_number)
    rescue StandardError
      return 'No such bus'
    end
    @buses_in_park << bus
    @buses_on_route.delete(bus)
    nil
  end

  def search_driver(name, surname)
    bus_for_driver = @buses_in_park.select do |bus|
      (bus.driver.name == name) && (bus.driver.surname == surname)
    end
    return 'No such driver' if bus_for_driver.count.zero?
    p bus_for_driver
    bus_for_driver = bus_for_driver[0]
    rfd = @routes.select { |r| r.number == bus_for_driver.route }
    rfd.count.zero? ? 'No such driver' : [bus_for_driver, rfd]
  end
end
