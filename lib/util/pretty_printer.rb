require 'rainbow/ext/string'

# Prints string using colors
module PrettyPrinter
  TYPES = {
    error: ->(string) { format(['ERROR: ', string], :red, :white) },
    success: ->(string) { format(['SUCCESS: ', string], :green, :white) },
    info: ->(string) { format(['INFO: ', string], :blue, :white) }
  }.freeze

  def self.format(arr, bg, color)
    arr.map { |a| a.background(bg).color(color) }.reduce(:<<)
  end

  def self.puts_f(type, string)
    puts TYPES[type].call(string)
  end
end
