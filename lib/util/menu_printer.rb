require 'text-table'
require_relative 'input_reader'

# Performs menu printing in a table-like pseudographic layout
module MenuPrinter
  MENU_ITEMS = ['Add bus from park to route',
                'Remove bus from route to park',
                'Choose list and observe',
                'Add route',
                'Add bus',
                'Buses availability',
                'Search driver by credentials',
                'Cost of gasoline consumption'].freeze

  def self.construct_menu
    menu_header = [{ value: 'Menu', colspan: 2 }]
    menu_rows = MENU_ITEMS.each_with_index.map do |item, i|
      [(i + 1).to_s << '.', item]
    end
    menu_rows << %w[0. Exit]
    { head: menu_header, rows: menu_rows }
  end

  MENU = Text::Table.new(construct_menu)

  def self.print_menu
    puts MENU
    yield InputReader.read_until_valid { |input| !/^[0-8]$/.match(input).nil? }
  end
end
