# Reads user input
module InputReader
  def self.read_raw
    $stdin.gets.chomp
  end

  def self.read_until_valid
    loop do
      input = read_raw
      puts 'Wrong input'.error unless yield input
      return input
    end
  end

  def self.ask(question, &block)
    puts question
    read_until_valid(&block)
  end
end
