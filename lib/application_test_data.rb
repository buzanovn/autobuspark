require_relative 'application'

# Test data injection for real application class
class Application
  def add_test_data
    allowed_numbers_of_buses =
      @bus_park_controller.buses_in_park.map(&:number)
    number = allowed_numbers_of_buses.count
    interval = (Float(number) / 4).round..(Float(number) / 2).round
    allowed_numbers_of_buses
      .take(Random.new.rand(interval))
      .each { |b| @bus_park_controller.add_bus_on_route b }
  end

  def apply_test_data
    puts 'Adding test data'.info
    if @bus_park_controller.buses_on_route.count > 0
      @bus_park_controller.buses_on_route.clear
    else
      add_test_data
    end
  end
end
