require_relative 'printable_model'

# Describes a model of a bus
class Bus < PrintableModel
  attr_accessor :number, :petrol_consumption, :driver, :route

  def initialize(number, petrol_consumption, driver, route)
    @number = Integer(number)
    @petrol_consumption = Float(petrol_consumption)
    @driver = driver
    @route = Integer(route)
  end

  def to_s
    (Text::Table.new head: [{ value: 'Bus', colspan: 2 }],
                     rows: headers.zip(to_a)
    ).to_s
  end

  def headers
    ['Number', 'Petrol Consumption', 'Driver', 'Route']
  end

  def to_a
    [@number, @petrol_consumption, @driver.to_s, @route]
  end
end
