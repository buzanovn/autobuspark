# Describes a model of a route
class Route < PrintableModel
  attr_accessor :number, :length, :number_of_buses

  def initialize(number, length, number_of_buses)
    @number = number
    @length = length
    @number_of_buses = number_of_buses
  end

  def to_s
    (Text::Table.new head: [{ value: 'Route', colspan: 2 }],
                     rows: headers.zip(to_a)
    ).to_s
  end

  def headers
    ['Number', 'Route length', 'Number of buses']
  end

  def to_a
    [@number, @length, @number_of_buses]
  end
end
