# Describes a model of driver
class Driver
  attr_accessor :name, :surname

  def initialize(name, surname)
    @name = name
    @surname = surname
  end

  def to_s
    "#{@surname} #{@name}"
  end
end
