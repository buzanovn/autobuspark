# Base class defining a model that
# could be printed as table
class PrintableModel
  def headers; end

  def to_a; end
end
