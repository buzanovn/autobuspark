require_relative 'io/csv_workers'
require_relative 'bus_park_controller'
require_relative 'io/menu_printer'
require_relative 'io/model_table_printer'
require_relative 'application_test_data'

# Describes application behaviour
# according to user actions
class Application
  # Should only be used for testing purposes
  def on_add_bus_to_park
    ModelTablePrinter.puts_model_list @bus_park_controller.buses_in_park
    bus_number = UserInputValidator.acquire_bus_information
    result = @bus_park_controller.add_bus_on_route Integer(bus_number)
    if result.nil?
      puts 'Successfully added bus to the route'.success
      ModelTablePrinter.puts_model_list @bus_park_controller.buses_on_route
    else
      puts result.error
    end
  rescue Interrupt
    puts 'User exited'.info
  end

  def on_remove_bus_from_park
    puts 'Nothing to remove'.error if @bus_park_controller.buses_on_route.zero?
    ModelTablePrinter.puts_model_list @bus_park_controller.buses_on_route
    bus_number = UserInputValidator.acquire_bus_information
    result = @bus_park_controller.remove_bus_from_route Integer(bus_number)
    if result.nil?
      puts 'Successfully removed bus from the route'.success
      ModelTablePrinter.puts_model_list @bus_park_controller.buses_on_route
    else
      puts result.error
    end
  rescue Interrupt
    puts 'User exited'.info
  end

  def on_add_bus
    args = UserInputValidator.acquire_bus_information
    @bus_park_controller.add_bus Bus.new(
      *args.values_at(0, 1), Driver.new(*args.values_at(2, 3)), args[4]
    )
    puts 'Successfully added bus to the park'.success
  rescue Interrupt
    puts 'User exited'.info
  end

  def on_add_route
    args = UserInputValidator.acquire_route_information
    @bus_park_controller.add_route Route.new(*args)
    puts 'Successfully added route to the park'.success
  rescue Interrupt
    puts 'User exited'.info
  end

  def on_show_consumptions
    total_consumption = @bus_park_controller.total_consumption
    consumption_by_route = @bus_park_controller.consumption_by_route
    if !total_consumption.nil?
      puts "Total consumption: #{total_consumption.round(2)}"
    else
      puts 'Nothing to count'.error
    end
    if !consumption_by_route.nil?
      puts Text::Table.new(
        head: [{ value: 'Consumption by route', colspan: 2 }],
        rows: consumption_by_route.collect { |k, v| [k, v] }
      )
    else
      puts 'Nothing to count'.error
    end
  end

  def on_search_driver
    creds = UserInputValidator.acquire_driver_credentials
    result = @bus_park_controller.search_driver(*creds.split)
    if result.class == String
      puts result.error
    else
      puts "#{result[0]}\n#{result[1]}"
    end
  rescue Interrupt
    puts 'User exited'.info
    return
  end

  @list_options = {
    '0' => 'Exiting'.info,
    '1' => @bus_park_controller.buses_on_route,
    '2' => @bus_park_controller.buses_in_park,
    '3' => @bus_park_controller.routes
  }

  def on_show_lists
    MenuPrinter.print_lists_submenu
    option = InputReader.ask 'Choose option' do |input|
      !/^[0-3]$/.match(input).nil?
    end
    choice = @list_options[option]
    if choice.class == Array
      ModelTablePrinter.puts_model_list(choice)
    else
      puts(choice)
    end
  end

  def on_show_buses_enough
    route_number = UserInputValidator.acquire_route_number(@bus_park_controller)
    route = @bus_park_controller.routes
                                .select { |r| r.number == route_number }[0]
    buses_on_route = @bus_park_controller.buses_on_route
                                         .select { |b| b.route == route_number }
    puts "There are #{buses_on_route.count} buses on "
    +" route #{route_number}, that needs #{route.number_of_buses}:"
  end

  @actions = {
    't' => -> { apply_test_data },
    '1' => -> { on_add_bus_to_park },
    '2' => -> { on_remove_bus_from_park },
    '3' => -> { on_show_lists },
    '4' => -> { on_add_route },
    '5' => -> { on_add_bus },
    '6' => -> { on_show_buses_enough },
    '7' => -> { on_search_driver },
    '8' => -> { on_show_consumptions },
    '0' => lambda {
             puts 'Exiting application'.info
             exit
           }
  }

  def check_arguments(argv)
    return 0 unless argv.size != 2
    puts "Too #{argv.size > 2 ? 'much' : 'few'} arguments".error
    exit
  end

  def initialize(argv)
    check_arguments(argv)
    buses = CSVReader.read_buses_from_file argv[0]
    routes = CSVReader.read_routes_from_file argv[1]
    @bus_park_controller = BusParkController.new(routes, buses)
    loop do
      MenuPrinter.print_menu do |input|
        p input
        @actions[input].call
      end
    end
  end
end

module UserInputValidator
  def self.acquire_route_number(bus_park_controller)
    InputReader.ask 'Choose route number' do |input|
      is_integer = !/^[0-9]+$/.match(input).nil?
      route_exists =
        !bus_park_controller.routes.map(&:number).index(Integer(input)).nil?
      is_integer && route_exists
    end.to_i
  end

  def self.acquire_driver_credentials
    question = 'Enter drivers credentials in format \"Name Surname\"'
    InputReader.ask question do |input|
      !/[[:alpha:]]+\s[[:alpha:]]+$/.match(input).nil?
    end
  end

  def acquire_bus_number
    InputReader.ask 'Enter the number of the bus' do |input|
      !/^[0-9]+$/.match(input).nil?
    end
  end

  def self.acquire_bus_information
    number = InputReader.ask_regex('Enter the number of the bus', /^[0-9]+$/)
    petrol_consumption = InputReader.ask_regex(
      'Enter petrol consumption of route',
      /^([[:digit:]]+)(?:\.[[:digit:]]{1,2})?$/
    )
    creds = acquire_driver_credentials
    route = InputReader.ask_regex('Enter the number of route', /^[0-9]+$/)
    [number, petrol_consumption, *creds, route]
  end

  def self.acquire_route_information
    number = InputReader.ask 'Enter the number of route' do |input|
      !/^[0-9]+$/.match(input).nil?
    end
    length = InputReader.ask 'Enter the length of route' do |input|
      !/^([[:digit:]]+)(?:\.[[:digit:]]{1,2})?$/.match(input).nil?
    end
    number_of_buses = InputReader.ask 'Enter the number of buses' do |input|
      Integer(input) > 0
    end
    [number, length, number_of_buses]
  end
end
