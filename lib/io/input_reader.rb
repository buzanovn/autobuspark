# Reads input and makes necessary validation
module InputReader
  def self.read_raw
    $stdin.gets.chomp
  end

  def self.read_until_valid
    loop do
      input = read_raw
      puts 'Wrong input'.error unless yield input
      return input
    end
  end

  def self.ask(question, &block)
    puts question
    read_until_valid(&block)
  end

  def self.ask_regex(question, regex)
    puts question
    read_until_valid do |input|
      !regex.match(input).nil?
    end
  end
end
