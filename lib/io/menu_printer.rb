require 'text-table'
require_relative '../io/input_reader'

# Prints menu and various submenus
module MenuPrinter
  MENU_ITEMS = ['Add bus from park to route',
                'Remove bus from route to park',
                'Choose list and observe',
                'Add route',
                'Add bus',
                'Buses availability',
                'Search driver by credentials',
                'Cost of petrol consumption'].freeze

  def self.construct_menu(header, items)
    menu_header = [{ value: header, colspan: 2 }]
    menu_rows = (1..items.count).map { |i| i.to_s + '.' }.zip items
    menu_rows << %w[0. Exit]
    { head: menu_header, rows: menu_rows }
  end

  MENU = Text::Table.new(construct_menu('Menu', MENU_ITEMS))

  def self.print_menu
    puts MENU
    menu_choice = InputReader.read_until_valid do |input|
      !/^([0-8])|(t)$/.match(input).nil?
    end
    yield menu_choice
  end

  def self.print_submenu(header, items)
    puts Text::Table.new(construct_menu(header, items))
  end

  def self.print_lists_submenu
    print_submenu('Lists', ['Buses on route', 'Buses in park', 'Routes'])
  end
end
