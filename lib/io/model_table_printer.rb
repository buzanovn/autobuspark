require 'rainbow/ext/string'
require 'text-table'

# Prints an array of models as table
module ModelTablePrinter
  def self.puts_model_list(list)
    if !list.count.zero?
      headers = list[0].headers
      rows = list.map(&:to_a)
      puts Text::Table.new head: headers, rows: rows
    else
      puts 'Nothing to print, list is empty'.info
    end
  end
end

# Logic for string formatting with colorizing the strings
module StringFormatter
  TYPES = {
    error: ->(string) { apply(['ERROR: ', string], :red, :white) },
    success: ->(string) { apply(['SUCCESS: ', string], :green, :white) },
    info: ->(string) { apply(['INFO: ', string], :blue, :white) },
    test: ->(string) { apply(['TEST', string], :yellow, :white) }
  }.freeze

  def self.apply(arr, bg, color)
    arr.map { |e| e.background(bg).color(color) }.reduce(:<<)
  end

  def self.format(type, string)
    TYPES[type].call(string)
  end
end

# Mixin used for formatting a string
# with a color according to type
class String
  def __format(type)
    StringFormatter.format(type, self)
  end

  def error
    __format :error
  end

  def info
    __format :info
  end

  def success
    __format :success
  end
end
