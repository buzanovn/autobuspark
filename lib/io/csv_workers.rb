require 'csv'
require_relative '../model/bus'
require_relative '../model/driver'
require_relative '../model/route'

# Performs reading from csv file
# with model extraction
module CSVReader
  def self.read_file(filename)
    array = []
    CSV.foreach(filename, headers: true) do |row|
      array.push yield row
    end
    array
  end

  def self.read_buses_from_file(filename)
    read_file filename do |row|
      Bus.new(Integer(row[0]), Float(row[1]),
              Driver.new(row[2].to_s, row[3].to_s), Integer(row[4]))
    end
  end

  def self.read_routes_from_file(filename)
    read_file filename do |row|
      Route.new(Integer(row[0]), Float(row[1]), Integer(row[2]))
    end
  end
end

# Performs serializing model
# to csv file
module CSVWriter
  def self.write_to_file(filename, &block)
    CSV.open(filename, 'a+') do |csv|
      csv << (yield block)
    end
  end

  def self.append_buses(filename, bus)
    write_to_file filename do
      [bus.number, bus.petrol_consumption, bus.driver.name,
       bus.driver.surname, bus.route]
    end
  end
end
